from fabric.api import run, local, cd, put, env
from fabric.operations import prompt, get

"""
fabric && dropbear sftp
http://wiki.openwrt.org/doc/howto/sftp.server
http://wiki.openwrt.org/inbox/replacingdropbearbyopensshserver
"""

env.use_ssh_config = True
env.shell = "/bin/ash -l -c"
# Set default hosts
if not env.hosts:
    env.hosts = ["root@carabolas.local"]

def upload():
    """ Uploads source/ to carambola:/tmp/source/ """
    local("tar -czf /tmp/carapyo.tar.gz carapyo")
    put("/tmp/carapyo.tar.gz", "/tmp/")
    with cd("/tmp"):
        run("tar -xzf carapyo.tar.gz")

def upload_test():
    """ Uploads to carambola:/tmp/carapyo and run tests """
    pass