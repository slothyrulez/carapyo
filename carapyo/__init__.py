#!/usr/bin/env python

#__author__ = 'slothy'
# RPIO
# raspberry original

import os
import atexit
from pins import PAD_GPIOS

"""
Information about Carambola2 gpio pins
http://wiki.openwrt.org/toh/8devices/carambola2
FREE GPIO PORTS: 18,19,20,21,22,23
lsof
/sys/kernel/debug/gpio
"""

# TUPLE 0 INDEXED (+1 port)
#J12 = (None,None,None,None,None,None,None,None,'11','12','18','19','20','21','22','23')
#J12 = (None,None,None,None,None,None,None,None,'11','12','18','19','20','21','22','23')
#J13 = ()

_HI = "hi"
_LO = "low"
_IN = "in"
_OUT = "out"

_VALID_PORT_DIRECTIONS = [_HI, _LO, _IN, _OUT]

class InvalidDirectionException(Exception):
    """ 
    Exception: Invalid port direction
    :raises InvalidDirectionException: Valid port directions are: hi, low, in, out
    """
    def __init__(self, direction):
        self.direction = direction

    def __str__(self):
        return u"Invalid GPIO port direction " + repr(self.direction) + "\n Valid directions: %s" % ",".join(_VALID_PORT_DIRECTIONS)

class InvalidPortException(Exception):
    """
    Exception: Invalid GPIO port for AR9331
    :raises InvalidPinException: Invalid GPIO port
    """
    def __init__(self, port):
        self.port = port

    def __str__(self):
        return u"Invalid GPIO port " + repr(self.port) + "\n For valid GPIO ports use show_ports"

class ClaimedPortException(Exception):
    """
    Exception: The GPIO port
    """
    def __init__(self, port):
        self.port = port

    def __str__(self):
        return u"Claimed GPIO port " + repr(self.port) + "\n Is the port being used?"

class NotSetupPortException(Exception):
    """
    Exception: GPIO port not exported via sysfs
    :raises NotSetupPortException:
    """
    def __init__(self, port):
        self.port = port

    def __str__(self):
        return u"No setup for GPIO port " + repr(self.port)

class GPIO(object):
    _CARAMBOLA2_PINS = None
    _SYSFS_PATH = "/sys/class/gpio"
    _EXPORTED_PORTS = {}

    def __new__(cls, *args, **kwargs):
        cls._CARAMBOLA2_PINS = PAD_GPIOS
        return object.__new__(cls, *args, **kwargs)

    def __init__(self):
        # import ipdb
        # ipdb.set_trace()
        pass

    def __str__(self):
        return self._CARAMBOLA2_PINS

    def _unexport(self, port):
        """
        Unexport sysfs gpio
        """
        with open(os.path.join(self._SYSFS_PATH, "unexport"), "w") as f:
            f.write(port)

    def _export(self, port):
        """
        Export sysfs gpio
        """
        with open(os.path.join(self._SYSFS_PATH, "export"), "w") as f:
            import ipdb
            ipdb.set_trace()
            #try:
                #f.write(port)
            #except:
                #raise ClaimedPortException(port)
            f.write(port)

    def _direction(self, port, direction):
        """
        Set I/O direction
        """
        if direction not in _VALID_PORT_DIRECTIONS:
            raise InvalidDirectionException(direction)
        with open(os.path.join(self._SYSFS_PATH, "gpio%s" % port, "direction"), "w") as f:
            f.write(direction)

    def _get_gpio_port(self, port):
        """
        Returns the valid GPIO port from the carambola 2 development board pin
        :param port: Pin numbered port
        :type port: int
        :return: GPIO String port
        """
        try:
            return self._CARAMBOLA2_PINS[port[0]][port[1]]["gpio"]
        except:
            raise InvalidPortException(port)


    def setup(self, port, direction):
        """
        Setup a specific port
        port
        direction
        high - Gpio to output starting value 1
        low - Gpio to output starting value 0
        out - low
        in - Gpio to input
        :return:
        """
        port = self._get_gpio_port(port)
        if port in self._EXPORTED_PORTS:
            self._unexport(port)
        self._export(port)
        self._direction(port, direction)
        self._EXPORTED_PORTS[port] = direction

        #raise InvalidPinException(port)

    def output(self, port):
        """
        Write to GPIO port
        :param port: Valid ort to output
        :return: Port value
        """
        port = self._get_gpio_port(port)
        if port not in self._EXPORTED_PORTS:
            raise NotSetupPortException(port)
        with open(os.path.join(self._SYSFS_PATH, "gpio%s" % port), "r") as f:
            return f.write()

    def input(self, port):
        """
        Read from GPIO
        :param port: Valid port to input
        :return:
        """
        port = self._get_gpio_port(port)
        if port not in self._EXPORTED_PORTS:
            raise NotSetupPortException(port)
        with open(os.path.join(self._SYSFS_PATH, "gpio%s" % port), "r") as f:
            return f.read(1)

    def show_ports(self):
        """
        Show a list of valid GPIO ports
        :return: None
        """
        print self.__str__()

    def get_port(self):
        """
        Return the list of valid GPIO ports
        :return: List
        """
        return self._CARAMBOLA2_PINS

    def get_exported(self):
        """
        Return the list of exported ports
        :return: List
        """
        return self._EXPORTED_PORTS


# def _unexport_all():
#     """
#     Unexport exported ports, this function is called at exit
#     """
#     _gpio = GPIO()
#     for port in _gpio._EXPORTED_PORTS:
#         _gpio._unexport(port)

#atexit.register(_unexport_all)

if __name__ == "__main__":
    gpio = GPIO()
    gpio.show_ports()
    try:
        print "FIRST SETUP"
        gpio.setup((0,11), "llal")
    except:
        pass
    print "SECOND SETUP"
    gpio.setup((0,11), "in")
